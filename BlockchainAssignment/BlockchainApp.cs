﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BlockchainAssignment.Wallet;

namespace BlockchainAssignment
{
    public partial class BlockchainApp : Form
    { //global variables for methods
        Blockchain blockchain;
        int pref = 0;
        Random rand;
        Transaction transaction;
        List<Transaction> Addresses;
        public BlockchainApp()
        { //initialize blockchain
            InitializeComponent();
            blockchain = new Blockchain();
            richTextBox1.Text = "New blockchain initialised!";


        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
        // Button click event handler to retrieve a block using the index input by the user

        private void printbutton_Click(object sender, EventArgs e)
        {
            // Retrieve user input into the index, allowing the selection of a specific block when generating blocks
            if (Int32.TryParse(indexText.Text, out int index))
            {
                richTextBox1.Text = blockchain.getBlockString(index);
            }

        }
        // Button click event handler for generating a wallet which creates a public and private key used later for validation
        private void generateWallet_Click(object sender, EventArgs e)
        {
            String privKey;
            Wallet.Wallet myWallet = new Wallet.Wallet(out privKey);
            PublicKey.Text = myWallet.publicID;
            PrivateKey.Text = privKey;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        // Button click event handler for validating the keys generated by the wallet
        private void validationKeys_Click(object sender, EventArgs e)
        {
            if (Wallet.Wallet.ValidatePrivateKey(PrivateKey.Text, PublicKey.Text))
            {
                richTextBox1.Text = "Keys are valid";
            }
            else
            {
                richTextBox1.Text = "Keys are invalid";
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
        //Create a new block
        private void generateBlock_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.getPendingTransaction();
            Block newblock = new Block(blockchain.getLastBlock(), transactions, PublicKey.Text);
            blockchain.Blocks.Add(newblock);
            richTextBox1.Text = blockchain.ToString();

        }

        // click button event handler for creating a transaction using wallet keys, which are then validated, allowing a transaction to proceed based on the user amount and fee

        private void createTransaction_Click(object sender, EventArgs e)
        {

            transaction = new Transaction(PublicKey.Text, ReceiverKey.Text, Double.Parse(Amount.Text), Double.Parse(Fee.Text), PrivateKey.Text);

            blockchain.transactionPool.Add(transaction);
            richTextBox1.Text = transaction.ToString();
        }

        private void ValidateChain_Click(object sender, EventArgs e)
        {

            bool valid = true;
            if (blockchain.Blocks.Count == 1)
            {
                if (!blockchain.validateMerkleRoot(blockchain.Blocks[0]))
                {
                    richTextBox1.Text = "Blockchain is invalid";

                }
                else
                {
                    richTextBox1.Text = "Blockchain is valid";
                }
                return;
            }
            for (int i = 1; i < blockchain.Blocks.Count - 1; i++)
            {
                if (blockchain.Blocks[i].prevHash != blockchain.Blocks[i - 1].hash || !blockchain.validateMerkleRoot(blockchain.Blocks[i]))
                {
                    richTextBox1.Text = "Blockchain is invalid";
                    return;
                }
            }


            if (valid)
            {
                richTextBox1.Text = "Blockchain is valid";
            }
            else
            {
                richTextBox1.Text = "Blockchain is invalid";
            }
        }

        private void CheckBalance_Click(object sender, EventArgs e)
        {  //to check the balance of coins
            richTextBox1.Text = blockchain.GetBalance(PublicKey.Text).ToString() + " Assignment Coin";
        }

        private void index_TextChanged(object sender, EventArgs e)
        {

        }
        private void Greedy_CheckedChanged(object sender, EventArgs e)
        {  // Update the preference based on whether the greedy checkbox is checked or unchecked
            if (Greedy.Checked == true)
            {
                pref = 1;
            }
            else if (Greedy.Checked != true)
            {
                pref = 0;
            }

        }

        private void Random_CheckedChanged(object sender, EventArgs e)
        // Update the preference based on whether the Random checkbox is checked or unchecked
        {
            //if checked
            if (Random.Checked == true)
            {
                pref = 2; // Set preference to 2 if random checkbox is checked
            }
            else if (Random.Checked != true)
            {
                pref = 0; // set preference to 0 if random checkbox is unchecked
            }
        }
        private void Altruistic_CheckedChanged(object sender, EventArgs e)
        {   // Update the preference based on whether the altruistic checkbox is checked or unchecked
            if (Altruistic.Checked == true)
            {
                pref = 3; // Set preference to 3 if alrutistic checkbox is checked
            }
            else if (Altruistic.Checked != true)
            {
                pref = 0; // set preference to 0 if alrutistic checkbox is unchecked
            }
        }

        private void Addressbox_CheckedChanged(object sender, EventArgs e)
        // Update the preference based on whether the addressbox checkbox is checked or unchecked

        {
            if (Addressbox.Checked == true)
            {
                pref = 4;
            }
            else if (Addressbox.Checked != true)
            {
                pref = 0;
            }
        }


        private void pendingTransactions_Click(object sender, EventArgs e)
        {
            // Define variables to use for random listing
            rand = new Random();
            blockchain.transactionPool.Sort((i, j) => j.timestamp.CompareTo(i.timestamp));
            int count = blockchain.transactionPool.Count;

            switch (pref)
            {
                // If greedy checkbox is checked, sort the transaction pool based on transaction fee in descending order
                case 1:
                    blockchain.transactionPool.Sort((i, j) => i.fee.CompareTo(j.fee));
                    blockchain.transactionPool.Reverse();
                    break;
                //if random box is checked, listing will always be randomised and shows a different list each time
                case 2:
                    while (count > 1)
                    {
                        count--; //count decrements, to avoid repeating random number
                        int random = rand.Next(count + 1);
                        Transaction temp = blockchain.transactionPool[random]; //swapping happens, temp to random, random to original count, count to random
                        blockchain.transactionPool[random] = blockchain.transactionPool[count];
                        blockchain.transactionPool[count] = temp;
                    }
                    break;


                // If altruistic checkbox is checked, sort the transaction pool based on the waiting time (timestamp) in ascending order
                case 3:
                    blockchain.transactionPool.Sort((i, j) => i.timestamp.CompareTo(j.timestamp));
                    richTextBox1.Text = String.Join("\n", blockchain.transactionPool);
                    break;

                default:
                    pref = 0;
                    break;
            }
            if (pref < 4)
            {   
                richTextBox1.Text = String.Join("\n", blockchain.transactionPool);
            }
            //used for the address mining prefernece
            if (pref == 4 && pref != 1 && pref != 2 && pref != 3)
            {
                //clear new address list
                Addresses.Clear();
                //loop transactions
                foreach (Transaction transaction in blockchain.transactionPool)
                {
                    String Address = ReceiverKey.Text;
                    //in case, receiver is equal to the sender, add to list and then output.
                    if (Address.Equals(transaction.senderAddress))
                    {
                        Addresses.Add(transaction);
                    }
                }
                richTextBox1.Text = String.Join("\n", Addresses);
            }
        }

        private void ReceiverKey_TextChanged(object sender, EventArgs e)
        {

        }
        // button to close the window
        private void exitbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

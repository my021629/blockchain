﻿

namespace BlockchainAssignment
{
   partial class BlockchainApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.printbutton = new System.Windows.Forms.Button();
            this.indexText = new System.Windows.Forms.TextBox();
            this.generateWallet = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.validationKeys = new System.Windows.Forms.Button();
            this.PublicKey = new System.Windows.Forms.TextBox();
            this.GenerateBlock = new System.Windows.Forms.Button();
            this.createTransaction = new System.Windows.Forms.Button();
            this.Amount = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Fee = new System.Windows.Forms.TextBox();
            this.PrivateKey = new System.Windows.Forms.TextBox();
            this.ReceiverKey = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ValidateChain = new System.Windows.Forms.Button();
            this.checkbalance = new System.Windows.Forms.Button();
            this.Greedy = new System.Windows.Forms.CheckBox();
            this.Random = new System.Windows.Forms.CheckBox();
            this.Altruistic = new System.Windows.Forms.CheckBox();
            this.pendingTransactions = new System.Windows.Forms.Button();
            this.Addressbox = new System.Windows.Forms.CheckBox();
            this.exitbutton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.InfoText;
            this.richTextBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.richTextBox1.Location = new System.Drawing.Point(12, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(657, 314);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // printbutton
            // 
            this.printbutton.Location = new System.Drawing.Point(11, 340);
            this.printbutton.Name = "printbutton";
            this.printbutton.Size = new System.Drawing.Size(92, 23);
            this.printbutton.TabIndex = 1;
            this.printbutton.Text = "Print";
            this.printbutton.UseVisualStyleBackColor = true;
            this.printbutton.Click += new System.EventHandler(this.printbutton_Click);
            // 
            // indexText
            // 
            this.indexText.Location = new System.Drawing.Point(110, 340);
            this.indexText.Name = "indexText";
            this.indexText.Size = new System.Drawing.Size(100, 20);
            this.indexText.TabIndex = 2;
            this.indexText.TextChanged += new System.EventHandler(this.index_TextChanged);
            // 
            // generateWallet
            // 
            this.generateWallet.Location = new System.Drawing.Point(560, 338);
            this.generateWallet.Name = "generateWallet";
            this.generateWallet.Size = new System.Drawing.Size(109, 46);
            this.generateWallet.TabIndex = 3;
            this.generateWallet.Text = "Generate Wallet";
            this.generateWallet.UseVisualStyleBackColor = true;
            this.generateWallet.Click += new System.EventHandler(this.generateWallet_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(341, 344);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Public Key";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(337, 376);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Private Key";
            // 
            // validationKeys
            // 
            this.validationKeys.Location = new System.Drawing.Point(560, 387);
            this.validationKeys.Name = "validationKeys";
            this.validationKeys.Size = new System.Drawing.Size(109, 28);
            this.validationKeys.TabIndex = 8;
            this.validationKeys.Text = "Validate Keys";
            this.validationKeys.UseVisualStyleBackColor = true;
            this.validationKeys.Click += new System.EventHandler(this.validationKeys_Click);
            // 
            // PublicKey
            // 
            this.PublicKey.Location = new System.Drawing.Point(404, 341);
            this.PublicKey.Name = "PublicKey";
            this.PublicKey.Size = new System.Drawing.Size(150, 20);
            this.PublicKey.TabIndex = 9;
            // 
            // GenerateBlock
            // 
            this.GenerateBlock.Location = new System.Drawing.Point(13, 369);
            this.GenerateBlock.Name = "GenerateBlock";
            this.GenerateBlock.Size = new System.Drawing.Size(90, 43);
            this.GenerateBlock.TabIndex = 11;
            this.GenerateBlock.Text = "Generate Block";
            this.GenerateBlock.UseVisualStyleBackColor = true;
            this.GenerateBlock.Click += new System.EventHandler(this.generateBlock_Click);
            // 
            // createTransaction
            // 
            this.createTransaction.Location = new System.Drawing.Point(13, 422);
            this.createTransaction.Name = "createTransaction";
            this.createTransaction.Size = new System.Drawing.Size(90, 48);
            this.createTransaction.TabIndex = 12;
            this.createTransaction.Text = "Create Transaction";
            this.createTransaction.UseVisualStyleBackColor = true;
            this.createTransaction.Click += new System.EventHandler(this.createTransaction_Click);
            // 
            // Amount
            // 
            this.Amount.Location = new System.Drawing.Point(470, 395);
            this.Amount.Name = "Amount";
            this.Amount.Size = new System.Drawing.Size(84, 20);
            this.Amount.TabIndex = 13;
            this.Amount.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(419, 402);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Amount";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(419, 431);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Fee";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // Fee
            // 
            this.Fee.Location = new System.Drawing.Point(470, 424);
            this.Fee.Name = "Fee";
            this.Fee.Size = new System.Drawing.Size(84, 20);
            this.Fee.TabIndex = 17;
            // 
            // PrivateKey
            // 
            this.PrivateKey.Location = new System.Drawing.Point(404, 369);
            this.PrivateKey.Name = "PrivateKey";
            this.PrivateKey.Size = new System.Drawing.Size(150, 20);
            this.PrivateKey.TabIndex = 10;
            // 
            // ReceiverKey
            // 
            this.ReceiverKey.Location = new System.Drawing.Point(378, 450);
            this.ReceiverKey.Name = "ReceiverKey";
            this.ReceiverKey.Size = new System.Drawing.Size(176, 20);
            this.ReceiverKey.TabIndex = 18;
            this.ReceiverKey.TextChanged += new System.EventHandler(this.ReceiverKey_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(301, 453);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Receiver Key";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // ValidateChain
            // 
            this.ValidateChain.Location = new System.Drawing.Point(560, 416);
            this.ValidateChain.Name = "ValidateChain";
            this.ValidateChain.Size = new System.Drawing.Size(109, 28);
            this.ValidateChain.TabIndex = 20;
            this.ValidateChain.Text = "Validate Chain";
            this.ValidateChain.UseVisualStyleBackColor = true;
            this.ValidateChain.Click += new System.EventHandler(this.ValidateChain_Click);
            // 
            // checkbalance
            // 
            this.checkbalance.Location = new System.Drawing.Point(109, 370);
            this.checkbalance.Name = "checkbalance";
            this.checkbalance.Size = new System.Drawing.Size(101, 42);
            this.checkbalance.TabIndex = 21;
            this.checkbalance.Text = "Check Balance";
            this.checkbalance.UseVisualStyleBackColor = true;
            this.checkbalance.Click += new System.EventHandler(this.CheckBalance_Click);
            // 
            // Greedy
            // 
            this.Greedy.AutoSize = true;
            this.Greedy.Location = new System.Drawing.Point(216, 344);
            this.Greedy.Name = "Greedy";
            this.Greedy.Size = new System.Drawing.Size(60, 17);
            this.Greedy.TabIndex = 23;
            this.Greedy.Text = "Greedy";
            this.Greedy.UseVisualStyleBackColor = true;
            this.Greedy.CheckedChanged += new System.EventHandler(this.Greedy_CheckedChanged);
            // 
            // Random
            // 
            this.Random.AutoSize = true;
            this.Random.Location = new System.Drawing.Point(276, 344);
            this.Random.Name = "Random";
            this.Random.Size = new System.Drawing.Size(66, 17);
            this.Random.TabIndex = 24;
            this.Random.Text = "Random";
            this.Random.UseVisualStyleBackColor = true;
            this.Random.CheckedChanged += new System.EventHandler(this.Random_CheckedChanged);
            // 
            // Altruistic
            // 
            this.Altruistic.AutoSize = true;
            this.Altruistic.Location = new System.Drawing.Point(216, 367);
            this.Altruistic.Name = "Altruistic";
            this.Altruistic.Size = new System.Drawing.Size(65, 17);
            this.Altruistic.TabIndex = 25;
            this.Altruistic.Text = "Altruistic";
            this.Altruistic.UseVisualStyleBackColor = true;
            this.Altruistic.CheckedChanged += new System.EventHandler(this.Altruistic_CheckedChanged);
            // 
            // pendingTransactions
            // 
            this.pendingTransactions.Location = new System.Drawing.Point(110, 422);
            this.pendingTransactions.Name = "pendingTransactions";
            this.pendingTransactions.Size = new System.Drawing.Size(100, 48);
            this.pendingTransactions.TabIndex = 27;
            this.pendingTransactions.Text = "List Pending";
            this.pendingTransactions.UseVisualStyleBackColor = true;
            this.pendingTransactions.Click += new System.EventHandler(this.pendingTransactions_Click);
            // 
            // Addressbox
            // 
            this.Addressbox.AutoSize = true;
            this.Addressbox.Location = new System.Drawing.Point(276, 367);
            this.Addressbox.Name = "Addressbox";
            this.Addressbox.Size = new System.Drawing.Size(64, 17);
            this.Addressbox.TabIndex = 28;
            this.Addressbox.Text = "Address";
            this.Addressbox.UseVisualStyleBackColor = true;
            this.Addressbox.CheckedChanged += new System.EventHandler(this.Addressbox_CheckedChanged);
            // 
            // exitbutton
            // 
            this.exitbutton.Location = new System.Drawing.Point(560, 445);
            this.exitbutton.Name = "exitbutton";
            this.exitbutton.Size = new System.Drawing.Size(109, 29);
            this.exitbutton.TabIndex = 29;
            this.exitbutton.Text = "Exit";
            this.exitbutton.UseVisualStyleBackColor = true;
            this.exitbutton.Click += new System.EventHandler(this.exitbutton_Click);
            // 
            // BlockchainApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(681, 481);
            this.Controls.Add(this.exitbutton);
            this.Controls.Add(this.Addressbox);
            this.Controls.Add(this.pendingTransactions);
            this.Controls.Add(this.Altruistic);
            this.Controls.Add(this.Random);
            this.Controls.Add(this.Greedy);
            this.Controls.Add(this.checkbalance);
            this.Controls.Add(this.ValidateChain);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ReceiverKey);
            this.Controls.Add(this.Fee);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Amount);
            this.Controls.Add(this.createTransaction);
            this.Controls.Add(this.GenerateBlock);
            this.Controls.Add(this.PrivateKey);
            this.Controls.Add(this.PublicKey);
            this.Controls.Add(this.validationKeys);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.generateWallet);
            this.Controls.Add(this.indexText);
            this.Controls.Add(this.printbutton);
            this.Controls.Add(this.richTextBox1);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "BlockchainApp";
            this.Text = "Blockchain App";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button printbutton;
        private System.Windows.Forms.TextBox indexText;
        private System.Windows.Forms.Button generateWallet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button validationKeys;
        private System.Windows.Forms.TextBox PublicKey;
        private System.Windows.Forms.Button GenerateBlock;
        private System.Windows.Forms.Button createTransaction;
        private System.Windows.Forms.TextBox Amount;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Fee;
        private System.Windows.Forms.TextBox PrivateKey;
        private System.Windows.Forms.TextBox ReceiverKey;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ValidateChain;
        private System.Windows.Forms.Button checkbalance;
        private System.Windows.Forms.CheckBox Greedy;
        private System.Windows.Forms.CheckBox Random;
        private System.Windows.Forms.CheckBox Altruistic;
        private System.Windows.Forms.Button pendingTransactions;
        private System.Windows.Forms.CheckBox Addressbox;
        private System.Windows.Forms.Button exitbutton;
    }
}


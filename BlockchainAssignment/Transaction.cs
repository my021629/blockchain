﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace BlockchainAssignment.Wallet
{
    class Transaction
    {
        // Global variables
        public String hash; // Hash of the transaction
        String signature; // Digital signature of the transaction
        public String senderAddress, recipientAddress; // Sender and recipient addresses
        public DateTime timestamp; // Timestamp of the transaction
        public double amount, fee; // Amount and fee of the transaction
        SHA256 hasher; // SHA256 hasher for hashing

        public Transaction(String from, String to, double amount, double fee, String privKey)
        {
            // Initializing  global variables for transaction input
            this.senderAddress = from;
            this.amount = amount;
            this.fee = fee;
            this.recipientAddress = to;
            this.timestamp = DateTime.Now;
            this.hash = CreateHash();
            // Creating signature using the sender address, private key,transaction hash
            this.signature = Wallet.CreateSignature(from, privKey, this.hash);
        }

        // to create the hash of the transaction
        public String CreateHash()
        {
            String hash = String.Empty;

            hasher = SHA256Managed.Create();
            // Hashing all the properties of the transaction
            String input = timestamp.ToString() + senderAddress + recipientAddress + amount.ToString() + fee.ToString();
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));

            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;
        }

        public override string ToString()
        {
   
            return
                "Transaction Hash: " + hash + "\n" +
                "Digital Signature: " + signature + "\n" +
                "Amount Transferred: " + amount.ToString() + " Assignment Coin." + "\n" +
                "Fees: " + fee.ToString() + "\n" +
                "Timestamp: " + timestamp + "\n" +
                "Sender Address: " + senderAddress + "\n" +
                "Receiver Address: " + recipientAddress + "\n";
        }
    }
}
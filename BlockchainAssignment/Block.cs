﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BlockchainAssignment.Wallet;
using System.Diagnostics;

namespace BlockchainAssignment
{
    class Block
    {
        // Global variables to hold block information
        DateTime timestamp; // timestamp of the block
        private int index;  // index position of the block
        public String hash; // hash value of the block
        public String prevHash; // hash value of the block
        public String newHash; // hash value of the block
        public String newHash2;// hash value of the block
        public Blockchain blockchain; // reference to the blockchai

        public List<Transaction> transactionList = new List<Transaction>(); // List of transactions in the block
        public String merkleRoot;  // Merkle root of the transactions

        Stopwatch timer;
        public TimeSpan timeTaken;

        // Proof of work variables
        public long nonce = 0;
        public long nonce1 = 0;
        public long nonce2 = 0;
        public int difficulty = 4;

        // Variables for multi-threading
        public bool thread1 = false;
        public bool thread2 = false;

        //fees and rewards
        public double reward = 1.0; //fixed logic
        public double fees = 0.0;

        public String minerAddress = String.Empty;

        // genesis block 
        public Block()
        {
            //initialising variables
            timestamp = DateTime.Now;
            index = 0;
            //hash
            prevHash = String.Empty;
            hash = Mine();
        }
        public Block(Block prevBlock, List<Transaction> list, String address = "")
        {

            // Creating a new block with the given previous block. list of transaction and miner address
            this.timestamp = DateTime.Now;
            this.index = prevBlock.index + 1;
            this.prevHash = prevBlock.hash;
            timer = new Stopwatch();
            minerAddress = address;

            list.Add(CreateRewardTransaction(list));
            transactionList = list;

            merkleRoot = MerkleRoot(transactionList);

            hash = Mine();


        }

        public Transaction CreateRewardTransaction(List<Transaction> transactions)
        {
            fees = transactions.Aggregate(0.0, (acc, t) => acc + t.fee);
            return new Transaction("Mine Rewards", minerAddress, (reward + fees), 0, " ");

        }
        //hashing function used to create the displayed hash
        public String createHash()
        {
            SHA256 hasher;
            hasher = SHA256Managed.Create();
            //concatenante block properties for hashing
            String input = index.ToString() + timestamp.ToString() + prevHash + nonce.ToString() + reward.ToString() + merkleRoot;
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));

            String hash = string.Empty;

            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;
        }
        public String createHashMultiThread(long nonce)
        {
            SHA256 hasher;
            hasher = SHA256Managed.Create();
            //concatenante block properties for hashing
            String input = index.ToString() + timestamp.ToString() + prevHash + nonce.ToString() + reward.ToString() + merkleRoot;
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));

            String hash = string.Empty;

            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;
        }

        public String Mine()
        {

            String hash = createHash();
            //Difficulty criteria definition


            // Creating a string of N (difficulty) 0's

            String r = new string('0', difficulty);

            while (!hash.StartsWith(r))
            {
                // Increment the nonce until the hash starts with the desired pattern
                nonce++;
                hash = createHash();
            }


            return hash;

        }

        // Mine method for multi-threading using thread 1 
        public void MineThread1()
        {

            String hash1 = createHashMultiThread(nonce1);
            //Difficulty criteria definition

            // Creating a string of N (difficulty) 0's
            String r = new string('0', difficulty);

            if (!hash1.StartsWith(r))
            {
                // If the hash not start with the desired pattern. set the newHash and thread1 variables
                newHash = hash1;
                thread1 = true;
            }
            //checking if thread 2 is active if so, this thread will sleep
            else if (thread2 == true)
            {
                Thread.Sleep(1);
            }
            else
            {
                nonce1 += 2;
            }
        }

        // Mine method for multi-threading using thread 2
        public void MineThread2()
        {
            String hash1 = createHashMultiThread(nonce2);
            //Difficulty criteria definition

            // Creating a string of N (difficulty) 0's
            String r = new string('0', difficulty);

            if (!hash1.StartsWith(r))
            {

                newHash2 = hash1;
                thread1 = true;
            }
            else if (thread1 == true)
            {
                Thread.Sleep(1);
            }
            else
            {
                nonce2 += 2;
            }
        }

        public String ThreadMine()
        {
            // Performing mining using two threads
            Thread thread1 = new Thread(MineThread1);
            Thread thread2 = new Thread(MineThread2);
            //start both threads
            thread1.Start();
            thread2.Start();

            // Creating a string of N (difficulty) 0's
            String r = new string('0', difficulty);
            // Checking if one of these threads is still working. If so, sleep for 10 milliseconds
            while (thread1.IsAlive || thread2.IsAlive)
            {
                Thread.Sleep(10);
            }
            if (hash.StartsWith(r))
            {
                nonce = nonce1;
                return newHash;
            }
            else
            {
                nonce = nonce2;
                return newHash2;
            }

        }

        //merkle root method for combining hashes
        public static String MerkleRoot(List<Transaction> transactionList)
        {
            List<String> hashes = transactionList.Select(t => t.hash).ToList();
            if (hashes.Count == 0)
            {
                return String.Empty;
            }
            if (hashes.Count == 1)
            {
                return HashCode.HashTools.combineHash(hashes[0], hashes[0]);
            }

            while (hashes.Count != 1)
            {
                List<String> merkleLeaves = new List<String>();
                for (int i = 0; i < hashes.Count; i += 2)
                {
                    if (i == hashes.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i]));
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i + 1]));
                    }
                }
                hashes = merkleLeaves;
            }
            return hashes[0];
        }


        // Convert block properties to string for output
        public override string ToString()
        {
            String output = String.Empty;
            transactionList.ForEach(t => output += (t.ToString() + "\n"));
            return "Index: " + index.ToString() + "\n" + "Time:  " + timestamp.ToString() + "\n" +
            "Previous Hash: " + prevHash + "\n" + "Hash: " +
            hash + "\n" + "Nonce: " + nonce + "\n" +
            "Transactions: \n" + output + "\n" +
            "Difficulty: " + difficulty.ToString() + "\n" +
            "Reward: " + reward.ToString() + "\n" +
            "Fees: " + fees.ToString() + "\n" +
            "Miner's Address: " + minerAddress + "\n";

        }
    }
}

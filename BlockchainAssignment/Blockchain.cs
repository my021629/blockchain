﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockchainAssignment.Wallet;

namespace BlockchainAssignment
{
    class Blockchain
    {
        public List<Block> Blocks = new List<Block>(); // List to store blocks in the blockchain
        public List<Transaction> transactionPool = new List<Transaction>();
        // Maximum number of transactions per block
        int transactionPerBlock = 5;
        public bool threadActive = false;

        public Blockchain()
        {
            //add the new block to list
            Blocks.Add(new Block());
        }
        public String getBlockString(int index)
        {
            //convert the list to string
            if (index >= 0 && index < Blocks.Count)
            {
                return Blocks[index].ToString();
            }
            else
            {
                return "No such block exists";
            }
        }
        //getter for last block
        public Block getLastBlock()
        {
            // Get the last block in the blockchain

            return Blocks[Blocks.Count - 1];
        }


        public List<Transaction> getPendingTransaction()
        {
            // Get a list of pending transactions to include in the next block
            int n = Math.Min(transactionPerBlock, transactionPool.Count);
            List<Transaction> transaction = transactionPool.GetRange(0, n);
            transactionPool.RemoveRange(0, n);
            return transaction;
        }
        public override string ToString()
        {
            // Get the string representation of the entire blockchain
            String output = String.Empty;

            Blocks.ForEach(b => output += (b.ToString() + "\n"));
            return output;
        }
        //getting balance method
        public double GetBalance(String address)
        {
            // Get the balance of a given wallet address

            double balance = 0.0;
            foreach (Block b in Blocks)
            {
                foreach (Transaction t in b.transactionList)
                {
                    if (t.recipientAddress.Equals(address))
                    {
                        balance += t.amount;
                    }
                    if (t.senderAddress.Equals(address))
                    {
                        balance -= (t.amount - t.fee);
                    }
                }
            }
            return balance;
        }
        // Validate the correctness of the Merkle root in a block
        public bool validateMerkleRoot(Block b)
        {
            String reMarkle = Block.MerkleRoot(b.transactionList);
            return reMarkle.Equals(b.merkleRoot);

        }
    }
}
